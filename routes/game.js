var express = require('express');
var router = express.Router();
var config = require('../config');

/* GET create game. */
router.get('/create', function(req, res, next) {
  var game_hash = Math.random().toString(36).substring(7);
  res.cookie(game_hash+'_host',true, { maxAge: 86400000 })
  res.redirect('/game/'+game_hash);
});

/* GET join game. */
router.get('/:game_hash', function(req, res, next) {
  var game_hash = req.params.game_hash;
  res.render('game', { game_hash: game_hash, title: config.title });
});

module.exports = router;
