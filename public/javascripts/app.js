function Game() {
  var game = this;

  this.init = function() {
    game.hash = window.game_hash;
    game.stage = game.getStage();
    game.player_id = game.getPlayerId();
    game.player_name = game.getPlayerName();
    game.storage_key = game.player_id+'_'+game.hash+'_';
    game.players = game.getPlayers();
    game.host = game.getHost();
    game.host_id = game.getHostId();
    game.clue_time = game.getClueTime();
    game.guess_time = game.getGuessTime();
    game.guesser = game.getGuesser();
    game.guesser_id = game.getGuesserId();
    game.choices = game.getChoices();
    game.word = game.getWord();
    game.clues = game.getClues();
    game.guess = game.getGuess();
    game.score = game.getScore();
    game.rounds = game.getRounds();
    game.timer = null;
  }

  // Getters and Setters

  this.getStage = function() {
    return getSessionItem('stage') || 'lobby';
  }

  this.setStage = function(stage) {
    game.stage = stage;
    setSessionItem('stage', stage);
  }

  this.getPlayerId = function() {
    var player_id = getCookie('player_id');
    if (!player_id) {
      player_id = Math.random().toString(36).substring(7);
      setCookie('player_id', player_id, 1);
    }
    return player_id;
  }

  this.getPlayerName = function() {
    return getCookie('player_name');
  }

  this.setPlayerName = function(player_name) {
    game.player_name = player_name;
    setCookie('player_name', player_name, 1)
  }

  this.getPlayers = function() {
    return getSessionJson('players') || {};
  }

  this.setPlayers = function(players) {
    game.players = players;
    setSessionJson('players', players);
  }

  this.addPlayer = function(player_id, player_name, html_only) {
    var html = '<li data-id="'+player_id+'"';
    if(game.host_id == player_id) {
      html += ' class="host"';
    }
    html += '>';
    html += player_name;
    html += '</li>';
    $('#players').append(html);
    if (!html_only) {
      var new_player = {};
      new_player[player_id] = player_name;
      var players = updateSessionJsonItem('players', new_player)
      game.players = players;
    }
    game.showStartGame();
  }

  this.playerOnline = function() {
    var online = false;
    Object.keys(game.players).forEach(function(prop, key){
      if (
        game.players[prop] == game.player_name &&
        prop == game.player_id
      ) {
        online = true;
      }
    });
    return online;
  }

  this.getHost = function() {
    var host = false;
    if(getCookie(game.hash+'_host')) {
      host = true;
      game.setHostId(game.player_id);
    }
    return host;
  }

  this.setHost = function(host) {
    game.host = host;
    if (host) {
      setCookie(game.hash+'_host', true, 1);
    }
    else {
      eraseCookie(game.hash+'_host');
    }
    game.setHostId(game.player_id);
  }

  this.getHostId = function() {
    return getSessionItem('host_id');
  }

  this.setHostId = function(host_id) {
    game.host_id = host_id;
    setSessionItem('host_id', host_id);
  }

  this.getClueTime = function() {
    return getSessionItem('clue_time') || 15;
  }

  this.setClueTime = function(clue_time) {
    game.clue_time = clue_time;
    setSessionItem('clue_time', clue_time);
  }

  this.getGuessTime = function() {
    return getSessionItem('guess_time') || 30;
  }

  this.setGuessTime = function(guess_time) {
    game.guess_time = guess_time;
    setSessionItem('guess_time', guess_time);
  }

  this.getGuesserId = function() {
    return getSessionItem('guesser_id');
  }

  this.getGuesser = function() {
    var guesser_id = game.getGuesserId();
    var guesser = false;
    if (game.player_id == guesser_id) {
      guesser = true;
    }
    return guesser;
  }

  this.setGuesser = function(guesser_id) {
    game.guesser_id = guesser_id;
    if (game.player_id == guesser_id) {
      game.guesser = true;
    }
    else {
      game.guesser = false;
    }
    setSessionItem('guesser_id', guesser_id);
    $('#players li').removeClass('guesser');
    $('#players li[data-id="'+guesser_id+'"]').addClass('guesser');
  }

  this.getChoices = function() {
    return getSessionJson('choices') || [];
  }

  this.setChoices = function(choices) {
    game.choices = choices;
    setSessionJson('choices', choices);
  }

  this.getWord = function() {
    return getSessionItem('word');
  }

  this.setWord = function(word) {
    game.word = word;
    setSessionItem('word', word);
    game.setClues({});
  }

  this.getClues = function() {
    return getSessionJson('clues') || {};
  }

  this.setClues = function(clues) {
    game.clues = clues;
    setSessionJson('clues', clues);
  }

  this.addClue = function(player_id, clue, html) {
    var new_clue = {};
    new_clue[player_id] = clue;
    var clues = updateSessionJsonItem('clues', new_clue);
    game.clues = clues;
    if (html && game.allCluesAdded()) {
      game.removeDuplicateClues();
      if (!game.cluesNull() && !game.oneClue()) {
        game.showClues();
        game.showAcceptClues();
      }
      else {
        socketEmit('accept_clues');
      }
    }
  }

  this.allCluesAdded = function() {
    return Object.keys(game.players).length == (Object.keys(game.clues).length + 1);
  }

  this.removeDuplicateClues = function() {
    var new_clues = {};
    Object.keys(game.clues).forEach(function(prop, key){
      var has_duplicate = false;
      if (game.clues[prop]) {
        Object.keys(game.clues).forEach(function(prop2, key2){
          if (
            game.clues[prop2] &&
            prop != prop2 &&
            game.clues[prop].toLowerCase() === game.clues[prop2].toLowerCase()
          ) {
            has_duplicate = true;
          }
        });
        if (has_duplicate) {
          new_clues[prop] = null;
        }
        else {
          new_clues[prop] = game.clues[prop];
        }
      }
    });
    game.clues = new_clues;
  }

  this.cluesNull = function() {
    var clues_null = true;
    Object.keys(game.clues).forEach(function(prop, key){
      if (game.clues[prop]) {
        clues_null = false;
      }
    });
    return clues_null;
  }

  this.oneClue = function() {
    var not_null_clues = 0;
    Object.keys(game.clues).forEach(function(prop, key){
      if (game.clues[prop]) {
        not_null_clues++;
      }
    });
    return not_null_clues === 1;
  }

  this.getGuess = function() {
    return getSessionItem('guess');
  }

  this.setGuess = function(guess) {
    game.guess = guess;
    setSessionItem('guess', guess);
  }

  this.getScore = function() {
    return getSessionItem('score') || 0;
  }

  this.incrementScore = function() {
    game.score++;
    setSessionItem('score', game.score);
    game.showScore();
  }

  this.getRounds = function() {
    return getSessionItem('rounds') || 0;
  }

  this.incrementRounds = function() {
    game.rounds++;
    setSessionItem('rounds', game.rounds);
    game.showRounds();
  }

  // Output

  this.showMessage = function(message) {
    $('#game').html(message);
  }

  this.showScore = function() {
    $('#score').html(game.score);
  }

  this.showRounds = function() {
    $('#rounds').html(game.rounds);
  }

  // Stages

  this.showSetPlayerName = function() {
    var player_name = game.player_name || '';
    html = '<form id="set_player_name">';
    html += '<div class="input-group">';
    html += '<input type="text" required class="form-control" id="player_name" value="'+player_name+'"/>';
    html += '<div class="input-group-append">';
    html += '<button type="submit" class="btn btn-primary">';
    html += 'Set name';
    html += '</button>';
    html += '</div>';
    html += '</div>';
    html += '</form>';
    $('#game').html(html);
    $('#set_player_name').on('submit', function(e) {
      e.preventDefault();
      var player_name = $('#player_name').val();
      game.setPlayerName(player_name);
      var data = {
        player_id: game.player_id,
        player_name: player_name
      }
      socketEmit('add_player', data);
      $(this).hide();
    });
    $('#player_name').focus();
  }

  this.showPlayers = function() {
    $('#players').html('');
    Object.keys(game.players).forEach(function(prop, key){
      game.addPlayer(prop, game.players[prop], true);
    });
  }

  this.showStartGame = function() {
    if (game.player_name) {
      if(
        Object.keys(game.players).length > 1 && 
        game.host
      ) {
        if ($('#start_game').length === 0) {
          var html = '';
          html += '<div class="text-center mt-3">';
          html += '<button id="start_game" class="btn btn-primary">Start game <i class="fas fa-play"></i></button>';
          html += '</div>';
          html += '<div class="col-sm-8 mt-5">';
          html += '<h5 class="pt-5">Settings:</h5>';
          html += '<form id="set_clue_time">';
          html += '<div class="input-group">';
          html += '<input type="number" required class="form-control" id="clue_time" value="'+game.clue_time+'"/>';
          html += '<div class="input-group-append">';
          html += '<button type="submit" class="btn btn-secondary">';
          html += 'Set clue time';
          html += '</button>';
          html += '</div>';
          html += '</div>';
          html += '</form>';
          html += '<form id="set_guess_time" class="mt-2">';
          html += '<div class="input-group">';
          html += '<input type="number" required class="form-control" id="guess_time" value="'+game.guess_time+'"/>';
          html += '<div class="input-group-append">';
          html += '<button type="submit" class="btn btn-secondary">';
          html += 'Set guess time';
          html += '</button>';
          html += '</div>';
          html += '</div>';
          html += '</form>';
          html += '</div>';
          $('#game').html(html);
          $('#start_game').on('click', function() {
            game.startGame();
          });
          $('#set_clue_time').on('submit', function(e) {
            e.preventDefault();
            var clue_time = $('#clue_time').val();
            socketEmit('set_clue_time', {clue_time: clue_time});
            $('#set_clue_time').replaceWith('<p class="alert alert-success">Clue time set</p>');
          });
          $('#set_guess_time').on('submit', function(e) {
            e.preventDefault();
            var guess_time = $('#guess_time').val();
            socketEmit('set_guess_time', {guess_time: guess_time});
            $('#set_guess_time').replaceWith('<p class="alert alert-success">Guess time set</p>');
          });
        }
      }
      else {
        if (game.host) {
          game.showMessage('Waiting for more players...');
        }
        else {
          game.showMessage('Waiting for '+game.players[game.host_id]+' to start the game...');
        }
      }
    }
  }

  this.startGame = function() {
    var random_player_id = Object.keys(game.players)[Math.floor(Math.random() * Object.keys(game.players).length)];
    game.emitGuesser(random_player_id);
  }

  this.emitGuesser = function(guesser_id) {
    game.setStage('showing_choices');
    socketEmit('set_guesser', {guesser_id:guesser_id});
    socketEmit('show_choices');
    $('#restart').css({display:'inline-block'}).on('click', function(e) {
      e.preventDefault();
      clearTimeout(game.timer);
      socketEmit('show_choices');
    });
  }

  this.showNumbers = function() {
    var html = '';
    html += '<h3>Choose a number</h3>';
    html += '<div id="choice_numbers" class="row justify-content-md-center">';
    for (i = 1; i <= 5; i++) {
      html += '<div class="choice_number col-md-4"><a href="#">'+i+'</a></div>';
    }
    html += '</div>';
    $('#game').html(html);
    $('#choice_numbers .choice_number a').on('click', function(e) {
      e.preventDefault();
      var number_div = this;
      setTimeout(function() {
        var choice_number = $(number_div).html();
        var choice_i = choice_number - 1;
        var word = game.choices[choice_i];
        socketEmit('set_word', {word: word});
      }, 500);
    });
  }

  this.showChoices = function() {
    var choices = game.choices;
    var html = '';
    html += '<h3>Choices</h3>';
    html += '<div id="choice_words" class="row justify-content-md-center">';
    for (var i in choices) {
      var choice = choices[i];
      html += '<div class="choice_word col-md-4"><div>'+choice+'</div></div>';
    }
    html += '</div>';
    html += 'Waiting for '+game.players[game.guesser_id]+' to choose...';
    $('#game').html(html);
  }

  this.showWord = function() {
    game.setStage('showing_word');
    var html = '';
    html += '<h2><span>Word:</span> '+game.word+'</h2>';
    if (game.clue_time != 0) {
      html += '<h1 id="countdown"><i class="fas fa-stopwatch"></i> <span>'+game.clue_time+'</span></h1>';
    }
    html += '<form id="set_clue">';
    html += '<div class="input-group">';
    html += '<input type="text" required class="form-control" id="clue" />';
    html += '<div class="input-group-append">';
    html += '<button type="submit" class="btn btn-primary">';
    html += 'Add clue';
    html += '</button>';
    html += '</div>';
    html += '</div>';
    html += '</form>';
    $('#game').html(html);
    $('#clue').focus();

    var after_set_html = '';
    after_set_html += '<h2><span>Word:</span> '+game.word+'</h2>';
    after_set_html += '<p>Waiting for other clues...</p>';

    if (game.clue_time != 0) {
      var clue_countdown = setInterval(function() {
        var current_counter = $('#countdown span').html();
        current_counter--;
        $('#countdown span').html(current_counter);
      }, 1000);

      game.timer = setTimeout(function() {
        clearInterval(clue_countdown);
        var data = {
          player_id: game.player_id,
          clue: null
        };
        socketEmit('add_clue', data);
      }, 1000*game.clue_time);
    }

    $('#set_clue').on('submit', function(e) {
      e.preventDefault();
      clearTimeout(game.timer);
      clearInterval(clue_countdown);
      var clue = $('#clue').val();
      var data = {
        player_id: game.player_id,
        clue: clue
      };
      socketEmit('add_clue', data);
      $('#game').html(after_set_html);
    });
    $('#clue').on('keyup', function(e) {
      var val = $(this).val().replace(/[^a-zA-Z-\s]/g, ' ').trim().split(' ')[0];
      $(this).val(val);
    });
  }

  this.showAcceptClues = function() {
    if(!game.guesser && $('#accept_clues').length === 0) {
      $('#game').append('<button id="accept_clues" class="btn btn-success">Accept clues <i class="fas fa-check"></i></button>');
      $('#game').append('<button id="reject_clues" class="btn btn-danger">Reject clues <i class="fas fa-times"></i></button>');
      $('#accept_clues').on('click', function() {
          socketEmit('accept_clues');
          $('#accept_clues').hide();
          $('#reject_clues').hide();
      });
      $('#reject_clues').on('click', function() {
          socketEmit('reject_clues');
          $('#accept_clues').hide();
          $('#reject_clues').hide();
      });
    }
  }

  this.showClues = function(set_guess, hide_cross) {
    game.setStage('showing_clues');
    game.removeDuplicateClues();
    var clues = game.clues;
    var html = '';
    if(!set_guess) {
      html += '<h2><span>Word:</span> '+game.word+'</h2>';
    }
    if(game.cluesNull()) {
      html += '<h3 class="mb-3">No clues!</h3>';
    }
    else {
      html += '<h3>Clues</h3>';
      html += '<ul id="clues">';
      Object.keys(clues).forEach(function(prop, key){
        if (clues[prop]) {
          html += '<li>';
          html += clues[prop];
          if(!set_guess && !hide_cross) {
            html += '<span class="cross" data-id="'+prop+'"><i class="fas fa-times"></i></span>';
          }
          html += '</li>';
        }
      });
      html += '</ul>';
    }
    if (hide_cross) {
      html += 'Waiting for '+game.players[game.guesser_id]+' to guess...';
    }
    if(set_guess) {
      if (game.guess_time != 0) {
        html += '<h1 id="countdown"><i class="fas fa-stopwatch"></i> <span>'+game.guess_time+'</span></h1>';
      }
      html += '<form id="set_guess">';
      html += '<div class="input-group">';
      html += '<input type="text" required class="form-control" id="guess" />';
      html += '<div class="input-group-append">';
      html += '<button type="submit" class="btn btn-primary">';
      html += 'Guess';
      html += '</button>';
      html += '</div>';
      html += '</div>';
      html += '</form>';
    }
    $('#game').html(html);
    $('#guess').focus();

    if(set_guess) {

      if (game.guess_time != 0) {
        var guess_countdown = setInterval(function() {
          var current_counter = $('#countdown span').html();
          current_counter--;
          $('#countdown span').html(current_counter);
        }, 1000);

        game.timer = setTimeout(function() {
          clearInterval(guess_countdown);
          socketEmit('set_guess', {guess: null});
        }, 1000*game.guess_time);
      }

      $('#set_guess').on('submit', function(e) {
        e.preventDefault();
        clearTimeout(game.timer);
        clearInterval(guess_countdown);
        var guess = $('#guess').val();
        socketEmit('set_guess', {guess: guess});
      });
      $('#guess').on('keyup', function(e) {
        var val = $(this).val().replace(/[^a-zA-Z-\s]/g, ' ').trim().split(' ')[0];
        $(this).val(val);
      });

    }
    else {
      $('#clues li span.cross').on('click', function(e) {
        e.preventDefault();
        var clue_id = $(this).data('id');
        socketEmit('remove_clue', {clue_id: clue_id});
      });
    }
  }

  this.showGuess = function() {
    game.setStage('showing_guess');
    var html = '';
    if (!game.guesser) {
      html += '<h2><span>Word:</span> '+game.word+'</h2>';
    }
    html += '<h2><span>Guess:</span> '+game.guess+'</h2>';
    if (!game.guesser) {
      html += '<button id="correct" class="btn btn-success">Correct enough <i class="fas fa-check"></i></button>';
      html += '<button id="wrong" class="btn btn-danger">Just plain wrong <i class="fas fa-times"></i></button>';
    }
    else {
      html += "Let's see if they'll give it to you...";
    }
    $('#game').html(html);
    if (!game.guesser) {
      $('#correct').on('click', function() {
        socketEmit('correct');
      });
      $('#wrong').on('click', function() {
        socketEmit('wrong');
      });
    }
  }

  this.wordAndGuess = function() {
    return '<h3><span>Word:</span> '+game.word+'</h3><h3><span>Guess:</span> '+game.guess+'</h3>';
  }

  this.correct = function() {
    game.showMessage('<h2 class="alert alert-success">'+game.players[game.guesser_id]+' got the right answer!</h2>'+game.wordAndGuess());
    game.incrementScore();
    game.incrementRounds();
    game.showNewRoundButton();
    game.setClues({});
  }

  this.wrong = function() {
    if (game.guess) {
      game.showMessage('<h2 class="alert alert-danger">Maybe next time '+game.players[game.guesser_id]+'...</h2>'+game.wordAndGuess());
    }
    else {
      game.showMessage("<h2 class='alert alert-danger'>"+game.players[game.guesser_id]+" didn't guess in time!</h2><h3><span>Word:</span> "+game.word+'</h3>');
    }
    game.incrementRounds();
    game.showNewRoundButton();
    game.setClues({});
  }

  this.showNewRoundButton = function() {
    game.setStage('finished_round');
    if (game.host) {
      $('#game').append('<br/><br/><button id="new_round" class="btn btn-primary">Start new round <i class="fas fa-play"></i></button>');
      $('#new_round').on('click', function() {
          game.newRound();
      });
      $('#restart').hide();
    }
  }

  this.newRound = function() {
    var current_guesser_id = game.getGuesserId();
    var found_current = false;
    var next_guesser_id = false;
    Object.keys(game.players).forEach(function(prop, key){
      if(found_current && !next_guesser_id) {
        next_guesser_id = prop;
      }
      if(!next_guesser_id && prop == current_guesser_id) {
        found_current = true;
      }
    });
    if (!next_guesser_id) {
      next_guesser_id = Object.keys(game.players)[0];
    }
    game.emitGuesser(next_guesser_id);
  }

};
var socket = io();
var game = new Game();
game.init();

$(document).ready(function() {
  if (game.player_name) {
    if (!game.playerOnline()) {
      var data = {
        player_id: game.player_id,
        player_name: game.player_name
      }
      socketEmit('add_player', data);
    }
    else {
      game.showStartGame();
    }
    game.showPlayers();
    game.showScore();
    game.showRounds();
  }
  else {
    game.showSetPlayerName();
  }
});

socket.on('connect', function() {
  socket.emit('join_game', game.hash);
});

socket.on('add_player', function(data) {
  if (game.host) {
    game.addPlayer(data.player_id, data.player_name);
    socketEmit('share_host_id', {host_id: game.player_id});
    socketEmit('share_players', {players: game.players});
  }
});

socket.on('share_players', function(host_players) {
  if (JSON.stringify(game.players) != JSON.stringify(host_players)) {
    game.setPlayers(host_players);
    game.showPlayers();
  }
});

socket.on('share_host_id', function(host_id) {
  game.setHostId(host_id);
});

socket.on('set_host', function() {
  game.setHost(true);
});

socket.on('set_clue_time', function(clue_time) {
  game.setClueTime(clue_time);
});

socket.on('set_guess_time', function(guess_time) {
  game.setGuessTime(guess_time);
});

socket.on('set_guesser', function(guesser_id) {
  game.setGuesser(guesser_id);
});

socket.on('show_choices', function(choices) {
  game.setChoices(choices);
  if (game.guesser) {
    game.showNumbers();
  }
  else {
    game.showChoices();
  }
});

socket.on('set_word', function(word) {
  game.setWord(word);
  if (game.guesser) {
    game.showMessage('Waiting for clues...');
  }
  else {
    game.showWord();
  }
});

socket.on('add_clue', function(data) {
  if (game.guesser) {
    game.addClue(data.player_id, data.clue);
  }
  else {
    game.addClue(data.player_id, data.clue, true);
  }
});

socket.on('remove_clue', function(clue_id) {
  if (game.guesser) {
    game.addClue(clue_id, null);
  }
  else {
    game.addClue(clue_id, null, true);
  }
});

socket.on('accept_clues', function() {
  if (game.guesser) {
    if (game.stage != 'showing_clues') {
      game.showClues(true);
    }
  }
  else {
    game.showClues(false, true);
  }
});

socket.on('reject_clues', function() {
  game.setClues({});
  if (game.guesser) {
    game.showClues(true);
  }
  else {
    game.showClues(false, true);
  }
});

socket.on('set_guess', function(guess) {
  game.setGuess(guess);
  if (!guess) {
    game.wrong();
  }
  else if (guess.toLowerCase() == game.word.toLowerCase()) {
    game.correct();
  }
  else if(game.host) {
    socketEmit('show_guess');
  }
});

socket.on('show_guess', function() {
  game.showGuess();
});

socket.on('correct', function() {
  game.correct();
});

socket.on('wrong', function() {
  game.wrong();
});

function socketEmit(event, data) {
  if (!data) {
    data = {};
  }
  socket.emit(event, {game_hash: game.hash,...data});
}

function getSessionItem(key) {
  return sessionStorage.getItem(game.storage_key+key);
}

function setSessionItem(key, data) {
  sessionStorage.setItem(game.storage_key+key, data);
}

function getSessionJson(key) {
  return JSON.parse(getSessionItem(key));
}

function setSessionJson(key, data) {
  setSessionItem(key, JSON.stringify(data));
}

function updateSessionJsonItem(session_key, session_value){
  let prevData = JSON.parse(sessionStorage.getItem(game.storage_key+session_key));
  if (prevData) {
    Object.keys(session_value).forEach(function(prop, key){
      prevData[prop] = session_value[prop];
    });
  }
  else {
    prevData = session_value;
  }
  sessionStorage.setItem(game.storage_key+session_key, JSON.stringify(prevData));
  return prevData;
}

function setCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {   
    document.cookie = name +'=; Path=/; Expires=Thu, 01 Jan 1970 00:00:01 GMT;';
}