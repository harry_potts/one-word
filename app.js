var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var config = require('./config');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var words = require('./words');

var indexRouter = require('./routes/index');
var gameRouter = require('./routes/game');

var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', indexRouter);
app.use('/game', gameRouter);
app.use(cookieParser());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.use(function(req, res, next){
  res.io = io;
  next();
});

io.on('connection', function(client){
  client.on('join_game', function(game_hash) {
    client.join(game_hash);
  });
  client.on('add_player', function(data) {
    io.in(data.game_hash).emit('add_player', data);
  });
  client.on('share_players', function(data) {
    client.to(data.game_hash).emit('share_players', data.players);
  });
  client.on('share_host_id', function(data) {
    client.to(data.game_hash).emit('share_host_id', data.host_id);
  });
  client.on('set_clue_time', function(data) {
    io.in(data.game_hash).emit('set_clue_time', data.clue_time);
  });
  client.on('set_guess_time', function(data) {
    io.in(data.game_hash).emit('set_guess_time', data.guess_time);
  });
  client.on('set_guesser', function(data) {
    io.in(data.game_hash).emit('set_guesser', data.guesser_id);
  });
  client.on('show_choices', function(data) {
    var choices = [];
    for(i = 1;i <= 5;i++) {
      choices.push(words[Math.floor(Math.random() * words.length)]);
    }
    io.in(data.game_hash).emit('show_choices', choices);
  });
  client.on('set_word', function(data) {
    io.in(data.game_hash).emit('set_word', data.word);
  });
  client.on('add_clue', function(data) {
    io.in(data.game_hash).emit('add_clue', data);
  });
  client.on('remove_clue', function(data) {
    io.in(data.game_hash).emit('remove_clue', data.clue_id);
  });
  client.on('accept_clues', function(data) {
    io.in(data.game_hash).emit('accept_clues');
  });
  client.on('reject_clues', function(data) {
    io.in(data.game_hash).emit('reject_clues');
  });
  client.on('set_guess', function(data) {
    io.in(data.game_hash).emit('set_guess', data.guess);
  });
  client.on('show_guess', function(data) {
    io.in(data.game_hash).emit('show_guess');
  });
  client.on('correct', function(data) {
    io.in(data.game_hash).emit('correct');
  });
  client.on('wrong', function(data) {
    io.in(data.game_hash).emit('wrong');
  });
});

module.exports = {app: app, server: server};