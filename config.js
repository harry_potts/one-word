var config = {};

config.title = 'Just One Word';

config.web = {};
config.web.port = process.env.PORT || 3000;

module.exports = config;